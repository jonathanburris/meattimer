//
//  TimerDoneInterfaceController.swift
//  MeatTimer
//
//  Created by Jonathan Burris on 3/14/15.
//
//

import WatchKit
import Foundation


class TimerDoneInterfaceController: WKInterfaceController {

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    @IBAction func dismissButton() {
        self.dismissController()
    }
}
